cp ./docker-compose.dev.yml ./docker-compose.yml
docker-compose up -d --build;

cp ./app/.env.dev ./app/.env

docker-compose up -d --build;

./scripts/composer.sh install
./scripts/console.sh d:s:u --force

chmod 777 -R ./app/var
chmod 777 -R ./app/vendor
