<?php

namespace App\Tests;

use App\Component\DonationServiceComponent;
use App\Entity\Donation;
use App\Repository\DonationRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class DonationServiceComponentTest extends TestCase
{
    public function testSaveMethod()
    {
        $donation = new Donation();
        $donation
            ->setAmount(123)
            ->setEmail('test@test.ru')
            ->setMessage('Message longMessage longMessage longMessage longMessage longMessage longMessage long')
            ->setUsername('username');

        $donationRepo = $this->createMock(ObjectRepository::class);
        $donationRepo->expects($this->any())
            ->method('find')
            ->willReturn($donation);

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($donationRepo);

        $donationService = new DonationServiceComponent(
            $this->createMock(EntityManager::class),
            $this->createMock(DonationRepository::class)
        );

        $expected = true;

        try {
            $donationService->saveDonation($donation);
        } catch (\Throwable $exception) {
            $expected = false;
        }

        $this->assertTrue($expected);
    }
}
