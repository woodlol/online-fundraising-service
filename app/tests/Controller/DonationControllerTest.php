<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DonationControllerTest extends WebTestCase
{
    public function testShowDonationForm()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/donate/payment');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testShowStatistics()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/donate/statistics');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testShowThanksPage()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/donate/thanks');

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
