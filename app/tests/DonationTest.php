<?php

namespace App\Tests;

use App\Entity\Donation;
use PHPUnit\Framework\TestCase;

class DonationTest extends TestCase
{
    public function testAllFields()
    {
        $donation = new Donation();
        $donation
            ->setAmount(123)
            ->setEmail('test@test.ru')
            ->setMessage('Message longMessage longMessage longMessage longMessage longMessage longMessage long')
            ->setUsername('username');

        $this->assertIsNotString($donation->getAmount());
        $this->assertIsString($donation->getEmail());
        $this->assertIsString($donation->getUsername());
        $this->assertIsString($donation->getMessage());

        $donation->setMessage(null);

        $this->assertNull($donation->getMessage());
    }
}
