<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Donation[]
     *
     * @ORM\OneToMany(targetEntity=Donation::class, mappedBy="company")
     */
    private $donations;

    public function __construct()
    {
        $this->donations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Donation[]
     */
    public function getDonations(): Collection
    {
        return $this->donations;
    }

    /**
     * @param Donation $donation
     *
     * @return $this
     */
    public function addDonation(Donation $donation): self
    {
        if (!$this->donations->contains($donation)) {
            $this->donations[] = $donation;
            $donation->setCompany($this);
        }

        return $this;
    }

    /**
     * @param Donation $donation
     *
     * @return $this
     */
    public function removeDonation(Donation $donation): self
    {
        if ($this->donations->contains($donation)) {
            $this->donations->removeElement($donation);

            if ($donation->getCompany() === $this) {
                $donation->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function totalAmount(): int
    {
        $amount = 0;

        /** @var Donation $donation */
        foreach ($this->donations->getValues() as $donation) {
            $amount += $donation->getAmount();
        }

        return $amount;
    }
}
