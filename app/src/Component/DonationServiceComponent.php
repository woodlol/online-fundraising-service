<?php

namespace App\Component;

use App\Component\Doctrine\DateTimePeriod;
use App\Component\DTO\UserDonateDTO;
use App\Entity\Donation;
use App\Repository\DonationRepository;
use Doctrine\ORM\EntityManagerInterface;

class DonationServiceComponent
{
    public const MAX_TOP_USER = 10;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DonationRepository */
    private $donationRepo;

    /**
     * @param EntityManagerInterface $entityManager
     * @param DonationRepository     $donationRepo
     */
    public function __construct(EntityManagerInterface $entityManager, DonationRepository $donationRepo)
    {
        $this->entityManager = $entityManager;
        $this->donationRepo  = $donationRepo;
    }

    /**
     * @param Donation $donation
     */
    public function saveDonation(Donation $donation): void
    {
        $this->entityManager->persist($donation);
        $this->entityManager->flush();
    }

    /**
     * @return array|UserDonateDTO[]
     */
    public function getDataForUsersStatistic(): array
    {
        return $this->donationRepo->getDataForStatisticsTableUser();
    }

    /**
     * @return array
     */
    public function getDataForPeriods(): array
    {
        return [
            DateTimePeriod::THIS_MONTH => $this->donationRepo->getDataForPeriod(DateTimePeriod::createPeriodForMonth(DateTimePeriod::THIS_MONTH)),
            DateTimePeriod::LAST_MONTH => $this->donationRepo->getDataForPeriod(DateTimePeriod::createPeriodForMonth(DateTimePeriod::LAST_MONTH)),
        ];
    }

    /**
     * @return string
     */
    public function getJsonDataForDashboard(): string
    {
        $thisMonthPeriod = DateTimePeriod::createPeriodForMonth(DateTimePeriod::THIS_MONTH);

        $arrayDatesFromPeriod = $thisMonthPeriod->getArrayDatesFromPeriod();

        foreach ($this->donationRepo->getDataForPeriod($thisMonthPeriod) as $donation) {
            foreach ($arrayDatesFromPeriod as $date => $item) {
                if ($date === $donation->getDateCreated()->format('d-m-Y')) {
                    $arrayDatesFromPeriod[$date] += $donation->getAmount();
                }
            }
        }

        return json_encode($arrayDatesFromPeriod);
    }
}
