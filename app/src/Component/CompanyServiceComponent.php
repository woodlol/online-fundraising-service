<?php

namespace App\Component;

use App\Entity\Company;
use App\Repository\CompanyRepository;

class CompanyServiceComponent
{
    /** @var CompanyRepository  */
    private $companyRepo;

    /**
     * @param CompanyRepository $companyRepo
     */
    public function __construct(CompanyRepository $companyRepo)
    {
        $this->companyRepo = $companyRepo;
    }

    /**
     * @return array|Company[]
     */
    public function getCompanies(): array
    {
        return $this->companyRepo->findAll();
    }
}
