<?php

namespace App\Component\Doctrine;

class DateTimePeriod
{
    public const LAST_MONTH = 'last';
    public const THIS_MONTH = 'this';

    /** @var \DateTime */
    private $dateFrom;

    /** @var \DateTime */
    private $dateTo;

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     */
    public function __construct(\DateTime $dateFrom, \DateTime $dateTo)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo   = $dateTo;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom(): \DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo(): \DateTime
    {
        return $this->dateTo;
    }

    /**
     * @param string $month
     *
     * @return static
     */
    public static function createPeriodForMonth(string $month): self
    {
        if (!in_array($month, [self::LAST_MONTH, self::THIS_MONTH], true)) {
            throw new \InvalidArgumentException('Add more checks');
        }

        return new self(
            new \DateTime("first day of $month month"),
            new \DateTime("last day of $month month")
        );
    }

    /**
     * @return array
     */
    public function getArrayDatesFromPeriod(): array
    {
        $res = [];

        $firstDay     = (int) $this->dateFrom->format('d');
        $lastDay      = (int) $this->dateTo->format('d');
        $monthAndYear = "-" . $this->dateTo->format('m-Y');

        for ($i = $firstDay; $i <= $lastDay; $i++) {
            $res[$i . $monthAndYear] = 0;
        }

        return $res;
    }
}
