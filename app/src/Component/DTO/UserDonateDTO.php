<?php

namespace App\Component\DTO;

class UserDonateDTO
{
    /** @var string */
    private $userName;

    /** @var string */
    private $email;

    /** @var int */
    private $allAmount;

    /**
     * @param string $userName
     * @param string $email
     * @param int    $allAmount
     */
    public function __construct(string $userName, string $email, int $allAmount)
    {
        $this->userName  = $userName;
        $this->email     = $email;
        $this->allAmount = $allAmount;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getAllAmount(): int
    {
        return $this->allAmount;
    }
}
