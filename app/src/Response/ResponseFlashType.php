<?php

namespace App\Response;

class ResponseFlashType
{
    public const SUCCESS = 'success';
    public const ERROR = 'danger';
}
