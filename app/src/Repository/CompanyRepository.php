<?php

namespace App\Repository;

class CompanyRepository extends DefaultEntityRepository
{
    /**
     * @return array
     */
    public function getTopCompanies(): array
    {
        return $this->createQueryBuilder('company')
            ->select('company.name, sum(company.donations.amount)')
            ->getQuery()
            ->getArrayResult();
    }
}
