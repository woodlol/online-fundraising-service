<?php

namespace App\Repository;

use App\Component\Doctrine\DateTimePeriod;
use App\Component\DonationServiceComponent;
use App\Component\DTO\DateDonateDTO;
use App\Component\DTO\UserDonateDTO;
use App\Entity\Donation;

class DonationRepository extends DefaultEntityRepository
{
    /**
     * @return array|UserDonateDTO[]
     */
    public function getDataForStatisticsTableUser(): array
    {
        return $this->createQueryBuilder('donation')
            ->select(
                'NEW ' . UserDonateDTO::class . '
                (
                donation.username,
                donation.email, 
                SUM(donation.amount) 
                )'
            )
            ->orderBy('SUM(donation.amount)', 'DESC')
            ->groupBy('donation.username, donation.email')
            ->setMaxResults(DonationServiceComponent::MAX_TOP_USER)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param DateTimePeriod $dateTimePeriod
     *
     * @return array|Donation[]
     */
    public function getDataForPeriod(DateTimePeriod $dateTimePeriod): array
    {
        return $this->createQueryBuilder('d')
            ->where('d.dateCreated BETWEEN :dateFrom and :dateTo')
            ->setParameters([
                'dateFrom' => $dateTimePeriod->getDateFrom()->format('Y-m-d H:i:s'),
                'dateTo'   => $dateTimePeriod->getDateTo()->format('Y-m-d H:i:s'),
            ])
            ->orderBy('d.amount', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
