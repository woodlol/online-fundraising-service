<?php

namespace App\Controller;

use App\Component\CompanyServiceComponent;
use App\Component\DonationServiceComponent;
use App\Form\DonationFormType;
use App\Response\ResponseFlashType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/donate", name="app_donate_")
 */
class DonationController extends AbstractController
{
    /**
     * @Route("/payment", name="payment")
     *
     * @param Request                  $request
     *
     * @param DonationServiceComponent $donationServiceComponent
     *
     * @return Response
     */
    public function payment(Request $request, DonationServiceComponent $donationServiceComponent): Response
    {
        $form = $this->createForm(DonationFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $donationServiceComponent->saveDonation($form->getData());

                return $this->redirectToRoute('app_donate_thanks');
            } catch (\Throwable $exception) {
                $this->addFlash(ResponseFlashType::ERROR, $exception->getMessage());
            }
        }

        return $this->render('donation/payment.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/statistics", name="statistics")
     *
     * @param DonationServiceComponent $donationServiceComponent
     * @param CompanyServiceComponent  $companyServiceComponent
     *
     * @return Response
     */
    public function statistics(
        DonationServiceComponent $donationServiceComponent,
        CompanyServiceComponent $companyServiceComponent
    ): Response {
        return $this->render('donation/statistics.html.twig', [
            'users'            => $donationServiceComponent->getDataForUsersStatistic(),
            'dates'            => $donationServiceComponent->getDataForPeriods(),
            'dataForDashBoard' => $donationServiceComponent->getJsonDataForDashboard(),
            'companies'        => $companyServiceComponent->getCompanies(),
        ]);
    }

    /**
     * @Route("/thanks", name="thanks")
     *
     * @return Response
     */
    public function thanksPage(): Response
    {
        return $this->render('donation/thanks-page.html.twig');
    }
}
