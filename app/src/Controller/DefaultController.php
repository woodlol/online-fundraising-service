<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="app_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/main", name="main_page")
     */
    public function main()
    {
        return $this->render('default/index.html.twig');
    }
}
