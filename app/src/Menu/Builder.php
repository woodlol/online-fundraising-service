<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class Builder
{
    /** @var FactoryInterface  */
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        $menu
            ->addChild('Home', ['route' => 'app_main_page'])
            ->getParent()
            ->addChild('Donate', ['route' => 'app_donate_payment'])
            ->getParent()
            ->addChild('Statistics', ['route' => 'app_donate_statistics'])
        ;

        return $menu;
    }
}
