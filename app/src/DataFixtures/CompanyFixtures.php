<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Common\Persistence\ObjectManager;

class CompanyFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Company::class, 5, function (Company $donation) {
            $donation->setName($this->faker->company);
        });

        $manager->flush();
    }
}
