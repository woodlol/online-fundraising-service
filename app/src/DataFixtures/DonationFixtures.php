<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Donation;
use Doctrine\Common\Persistence\ObjectManager;

class DonationFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $companies = $manager->getRepository(Company::class)->findAll();

        $this->createMany(Donation::class, 50, function (Donation $donation) use ($companies) {
            $donation
                ->setCompany($companies[rand(0, (count($companies) - 1))])
                ->setUsername($this->faker->name)
                ->setEmail($this->faker->email)
                ->setAmount($this->faker->numberBetween(10, 10000))
                ->setMessage($this->faker->text)
                ->setDateCreated($this->faker->dateTimeBetween('- 1 month'));
        });

        $manager->flush();
    }
}
