### Start

Install
[docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/),
[docker-compose](https://docs.docker.com/install/linux/docker-ce/ubuntu/),


git clone this project

First deploy 
Run from sudo 
```bash
./scripts/up-dev.sh
./scripts/console.sh doctrine:fix:load
```

### Stop

```bash
./scripts/down-dev.sh
```
